package com.wintoro.uas

data class MainModel ( val name: String, val positif: String, val sembuh: String,
                       val meninggal: String )