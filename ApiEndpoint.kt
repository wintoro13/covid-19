package com.wintoro.uas.retrofit

import com.wintoro.uas.MainModel
import retrofit2.Call
import retrofit2.http.*

interface ApiEndpoint {

    @GET("indonesia")
    fun getData(): Call<List<MainModel>>
}